package cz.martinvedra;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AppRun implements CommandLineRunner {
    private final Calculator calculator;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hello world");
        System.out.println(calculator.add(1, 2));
    }
}
