package cz.martinvedra;

import org.springframework.stereotype.Component;

@Component
public class Calculator {

    public Double add(double a, double b) {
        return a + b;
    }
}
