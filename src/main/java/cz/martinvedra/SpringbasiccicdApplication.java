package cz.martinvedra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbasiccicdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbasiccicdApplication.class, args);
    }

}
