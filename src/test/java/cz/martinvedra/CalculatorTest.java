package cz.martinvedra;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void addTest() {

        // given
        double a = 1;
        double b = 2;

        // when
        Double add = calculator.add(a, b);

        // then
        Assertions.assertEquals(3, add);

    }
}
